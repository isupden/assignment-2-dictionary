%define buffer_size 256
%include "colon.inc"
%include "lib.inc"
extern find_word
global _start

section .rodata
%include "words.inc"
not_found: db "Key not fount", 10, 0
invalid: db "Invalid key", 10, 0

section .bss
data_buffer: resb buffer_size

section .text
_start:
    mov rsi, buffer_size
	mov rdi, data_buffer
	call read_line
	test rax, rax
	je .print_invalid

    mov rsi, dict_link
    mov rdi, rax
    call find_word
    test rax, rax
    jz .fail

    mov rdi, rax
    call print_newline
    call print_string
    call exit


    .print_invalid:
        mov rdi, invalid
        call print_error
        call exit

    .fail:
        mov rdi, not_found
        call print_error
        call exit
