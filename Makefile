ASM=nasm
ASMFLAGS=-f elf64
LD=ld

dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o %@ %<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o %@ %<

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o %@ %<

main: *.o
	LD -o $@ $<

.PHONY: clean
clean:
	rm *.o