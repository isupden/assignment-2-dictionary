global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy
global print_error
global read_line

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdi, 1
    mov rax, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    mov r9, rsp
    dec rsp
    mov byte[rsp], 0
    .loop:
        xor rdx, rdx
        div r8
        add rdx, 0x30
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        jne .loop
    mov rdi, rsp
    push r9
    call print_string
    pop rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov al, byte[rdi]
        cmp al, byte[rsi]
        jne .not_equals
        inc rdi
        inc rsi
        cmp al, 0
        jne .loop
    mov rax, 1
    ret
    .not_equals:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rdi, rdi
    xor rdx, rdx		
	inc rdx	
    xor rax, rax
    push rax
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    .clean_whitespace:
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        cmp rax, 0x20
        je .clean_whitespace
        cmp rax, 0x9
        je .clean_whitespace
        cmp rax, 0xA
        je .clean_whitespace
    xor rdx, rdx
    .loop:
        cmp rax, 0x20
        je .end
        cmp rax, 0x9
        je .end
        cmp rax, 0xA
        je .end
        cmp rax, 0
        je .end
        mov byte[rdi+rdx], al
        inc rdx
        cmp rdx, rsi
        jae .error
        push rdi
		push rsi
		push rdx
		call read_char
		pop rdx
		pop rsi
		pop rdi
		jmp .loop
    .end:
        cmp rdx, rsi
		jae .error
		mov byte[rdi+rdx], 0
		mov rax, rdi
		ret
    .error:
        xor rax, rax
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor r8, r8
    mov r10, 10
    .loop:
        mov sil, [rdi + r8]
        cmp sil, 0
        je .end
        cmp sil, '0'
        jb .end
        cmp sil, '9'
        ja .end
        mul r10
        sub sil, '0'
        add rax, rsi
        inc r8
        jmp .loop
    .end:
        mov rdx, r8
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .neg
    jmp parse_uint
    .neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jge .else
    xor r8, r8
    .loop: 
        mov cl, [rdi + r8]
        mov [rsi + r8], cl
        inc r8
        cmp cl, 0
        je .end
        jmp .loop
    .else:
        xor rax, rax
    .end:
        ret

print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 2
    xor rax, rax
    inc rax
    syscall
    ret
    
read_line:
	push rdi
	push rsi
	mov rcx, rsi
	
	.loop:
		push rdi
		push rcx
		call read_char
		pop rcx
		pop rdi
		
		cmp al, 0xA
		je .ex
		
		cmp rcx, 1
		jna .error
		test rax, rax
		jz .ex
		
		mov byte[rdi], al
		inc rdi
		dec rdx
		jmp .error
		
	.ex:
		pop rdx
		pop rax
		sub rdx, rcx
		mov byte[rdi], 0
		ret
		
	.error:
		pop rdx
		pop rax
		mov rax, 0
		ret

