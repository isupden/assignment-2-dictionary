global find_word
extern string_equals
extern string_length
%define eight 8
section .text

find_word:
    test rsi, rsi
    je .fail
    .loop:
    	push rdi
    	push rsi
    	add rsi, eight
    	call string_equals
    	pop rsi
    	pop rdi
    	test rax, rax
    	jnz .getAddress
    	mov rsi, [rsi]
    	test rsi, rsi
    	je .fail
    	jmp .loop

    .fail:
        xor rax, rax
        ret
    
    .getAddress:
        push rsi
        call string_length
        pop rsi
        add rsi, 9
        add rax, rsi
        ret
