%macro colon 2
%define dict_link 0
%2:
	%ifid %2
	%else
		%error "Неудачное название метки"
	%endif
	%ifstr %1
	%else
		%error "Неудачное значение ключа"
	%endif

	%if dict_link != 0
		dq dict_link
	%else 
		dq 0
	%endif
	
	db %1, 0
	%define dict_link %2
%endmacro
